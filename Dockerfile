FROM alpine AS builder

RUN apk add --no-cache --update git;

WORKDIR /tmp/app-build

COPY [".git", "/tmp/app-build/.git"]

RUN git reset --hard \
    && git checkout master \
    && git checkout . \
    && rm -rf .git

# ======================================================== APP ========================================================================================================================
FROM nginx:mainline-alpine AS app

COPY --from=builder /tmp/app-build/public /usr/share/nginx/html/
COPY --from=builder /tmp/app-build/nginx.conf /etc/nginx/conf.d/default.conf

ENV DEFAULT_EMAIL         "webmaster@puharic.com"
ENV VIRTUAL_HOST          "puharic.com,www.puharic.com,aleksandar.puharic.com,alexander.puharic.com,puharic.se,www.puharic.se,aleksandar.puharic.se,alexander.puharic.se"
ENV LETSENCRYPT_EMAIL     "${DEFAULT_EMAIL}"
ENV LETSENCRYPT_HOST      "${VIRTUAL_HOST}"
ENV HTTPS_METHOD          noredirect

EXPOSE 80/TCP